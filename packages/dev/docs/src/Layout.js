/*
 * Copyright 2020 Adobe. All rights reserved.
 * This file is licensed to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR REPRESENTATIONS
 * OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

import ChevronLeft from "@spectrum-icons/ui/ChevronLeftLarge";
import clsx from "clsx";
import { Divider } from "@react-spectrum/divider";
import docStyles from "./docs.css";
import { getAnchorProps } from "./utils";
import heroImageAria from "url:../pages/assets/ReactAria_976x445_2x.png";
import heroImageHome from "url:../pages/assets/ReactSpectrumHome_976x445_2x.png";
import heroImageSpectrum from "url:../pages/assets/ReactSpectrum_976x445_2x.png";
import heroImageStately from "url:../pages/assets/ReactStately_976x445_2x.png";
import highlightCss from "./syntax-highlight.css";
import { ImageContext } from "./Image";
import { LinkProvider } from "./types";
import linkStyle from "@adobe/spectrum-css-temp/components/link/vars.css";
import { MDXProvider } from "@mdx-js/react";
import path from "path";
import React from "react";
import ruleStyles from "@adobe/spectrum-css-temp/components/rule/vars.css";
import sideNavStyles from "@adobe/spectrum-css-temp/components/sidenav/vars.css";
import { theme } from "@react-spectrum/theme-default";
import { ToC } from "./ToC";
import typographyStyles from "@adobe/spectrum-css-temp/components/typography/vars.css";
import { VersionBadge } from "./VersionBadge";

const TLD = "watheia.org";
const HERO = {
  "react-spectrum": heroImageSpectrum,
  "react-aria": heroImageAria,
  "react-stately": heroImageStately,
};

const mdxComponents = {
  h1: ({ children, ...props }) => (
    <h1
      {...props}
      className={clsx(
        typographyStyles["spectrum-Heading1--display"],
        typographyStyles["spectrum-Article"],
        docStyles["articleHeader"]
      )}
    >
      {children}
    </h1>
  ),
  h2: ({ children, ...props }) => (
    <>
      <h2
        {...props}
        className={clsx(
          typographyStyles["spectrum-Heading3"],
          docStyles["sectionHeader"],
          docStyles["docsHeader"]
        )}
      >
        {children}
        <span className={clsx(docStyles["headingAnchor"])}>
          <a
            className={clsx(
              linkStyle["spectrum-Link"],
              docStyles.link,
              docStyles.anchor
            )}
            href={`#${props.id}`}
            aria-label={`Direct link to ${children}`}
          >
            #
          </a>
        </span>
      </h2>
      <Divider marginBottom="33px" />
    </>
  ),
  h3: ({ children, ...props }) => (
    <h3
      {...props}
      className={clsx(
        typographyStyles["spectrum-Heading4"],
        docStyles["sectionHeader"],
        docStyles["docsHeader"]
      )}
    >
      {children}
      <span className={docStyles["headingAnchor"]}>
        <a
          className={clsx(
            linkStyle["spectrum-Link"],
            docStyles.link,
            docStyles.anchor
          )}
          href={`#${props.id}`}
          aria-label={`Direct link to ${children}`}
        >
          #
        </a>
      </span>
    </h3>
  ),
  p: ({ children, ...props }) => (
    <p className={typographyStyles["spectrum-Body3"]} {...props}>
      {children}
    </p>
  ),
  ul: ({ children, ...props }) => (
    <ul {...props} className={typographyStyles["spectrum-Body3"]}>
      {children}
    </ul>
  ),
  ol: ({ children, ...props }) => (
    <ol {...props} className={typographyStyles["spectrum-Body3"]}>
      {children}
    </ol>
  ),
  code: ({ children, ...props }) => (
    <code {...props} className={typographyStyles["spectrum-Code4"]}>
      {children}
    </code>
  ),
  inlineCode: ({ children, ...props }) => (
    <code {...props} className={typographyStyles["spectrum-Code4"]}>
      {children}
    </code>
  ),
  a: ({ children, ...props }) => (
    <a
      {...props}
      className={clsx(linkStyle["spectrum-Link"], docStyles.link)}
      {...getAnchorProps(props.href)}
    >
      {children}
    </a>
  ),
  kbd: ({ children, ...props }) => (
    <kbd {...props} className={docStyles["keyboard"]}>
      {children}
    </kbd>
  ),
};

const sectionTitles = {
  blog: "React Spectrum Blog",
  releases: "React Spectrum Releases",
};

function dirToTitle(dir) {
  return dir
    .split("/")[0]
    .split("-")
    .map((w) => w.charAt(0).toUpperCase() + w.slice(1))
    .join(" ");
}

function stripMarkdown(description) {
  return (description || "").replace(/\[(.*?)\]\(.*?\)/g, "$1");
}

function isBlogSection(section) {
  return section === "blog" || section === "releases";
}

function Page({ children, currentPage, publicUrl, styles, scripts }) {
  let parts = currentPage.name.split("/");
  let isBlog = isBlogSection(parts[0]);
  let isSubpage = parts.length > 1 && !/index\.html$/.test(currentPage.name);
  let pageSection = isSubpage ? dirToTitle(currentPage.name) : "React Spectrum";
  if (isBlog && isSubpage) {
    pageSection = sectionTitles[parts[0]];
  }

  let keywords = [
    ...new Set(
      currentPage.keywords
        .concat([currentPage.category, currentPage.title, pageSection])
        .filter((k) => !!k)
    ),
  ];
  let description =
    stripMarkdown(currentPage.description) ||
    `Documentation for ${currentPage.title} in the ${pageSection} package.`;
  let title =
    currentPage.title +
    (!/index\.html$/.test(currentPage.name) || isBlog
      ? ` – ${pageSection}`
      : "");
  let hero = (parts.length > 1 ? HERO[parts[0]] : "") || heroImageHome;
  let heroUrl = `https://${TLD}/${currentPage.image || path.basename(hero)}`;

  return (
    <html
      lang="en-US"
      dir="ltr"
      prefix="og: http://ogp.me/ns#"
      className={clsx(
        theme.global.spectrum,
        theme.light["spectrum--light"],
        theme.medium["spectrum--medium"],
        typographyStyles.spectrum,
        docStyles.provider,
        highlightCss.spectrum
      )}
    >
      <head>
        <title>{title}</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        {/* Server rendering means we cannot use a real <Provider> component to do this.
            Instead, we apply the default theme classes to the html element. In order to
            prevent a flash between themes when loading the page, an inline script is put
            as close to the top of the page as possible to switch the theme as soon as
            possible during loading. It also handles when the media queries update, or
            local storage is updated. */}
        <script
          dangerouslySetInnerHTML={{
            __html: `(() => {
            let classList = document.documentElement.classList;
            let style = document.documentElement.style;
            let dark = window.matchMedia('(prefers-color-scheme: dark)');
            let fine = window.matchMedia('(any-pointer: fine)');
            let update = () => {
              if (localStorage.theme === "dark" || (!localStorage.theme && dark.matches)) {
                classList.remove("${theme.light["spectrum--light"]}");
                classList.add("${theme.dark["spectrum--darkest"]}", "${docStyles.dark}");
                style.colorScheme = 'dark';
              } else {
                classList.add("${theme.light["spectrum--light"]}");
                classList.remove("${theme.dark["spectrum--darkest"]}", "${docStyles.dark}");
                style.colorScheme = 'light';
              }

              if (!fine.matches) {
                classList.remove("${theme.medium["spectrum--medium"]}", "${docStyles.medium}");
                classList.add("${theme.large["spectrum--large"]}", "${docStyles.large}");
              } else {
                classList.add("${theme.medium["spectrum--medium"]}", "${docStyles.medium}");
                classList.remove("${theme.large["spectrum--large"]}", "${docStyles.large}");
              }
            };

            update();
            dark.addListener(() => {
              delete localStorage.theme;
              update();
            });
            fine.addListener(update);
            window.addEventListener('storage', update);
          })();
        `.replace(/\n|\s{2,}/g, ""),
          }}
        />
        <link
          rel="preload"
          as="font"
          href="https://use.typekit.net/af/eaf09c/000000000000000000017703/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3"
          crossOrigin=""
        />
        <link
          rel="preload"
          as="font"
          href="https://use.typekit.net/af/cb695f/000000000000000000017701/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3"
          crossOrigin=""
        />
        <link
          rel="preload"
          as="font"
          href="https://use.typekit.net/af/505d17/00000000000000003b9aee44/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n9&v=3"
          crossOrigin=""
        />
        <link
          rel="preload"
          as="font"
          href="https://use.typekit.net/af/74ffb1/000000000000000000017702/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=i4&v=3"
          crossOrigin=""
        />
        {styles.map((s) => (
          <link rel="stylesheet" href={s.url} />
        ))}
        {scripts.map((s) => (
          <script type={s.type} src={s.url} defer />
        ))}
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:image" content={heroUrl} />
        <meta property="og:title" content={currentPage.title} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`https://${TLD}${currentPage.url}`} />
        <meta property="og:image" content={heroUrl} />
        <meta property="og:description" content={description} />
        <meta property="og:locale" content="en_US" />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Article",
              author: "Adobe Inc",
              headline: currentPage.title,
              description: description,
              image: heroUrl,
              publisher: {
                "@type": "Organization",
                url: "https://www.adobe.com",
                name: "Adobe",
                logo: "https://www.adobe.com/favicon.ico",
              },
            }),
          }}
        />
      </head>
      <body>
        {children}
        <script
          dangerouslySetInnerHTML={{
            __html: `
            window.addEventListener('load', () => {
              let script = document.createElement('script');
              script.async = true;
              script.src = 'https://assets.adobedtm.com/a7d65461e54e/01d650a3ee55/launch-4d5498348926.min.js';
              document.head.appendChild(script);
            });
          `,
          }}
        />
      </body>
    </html>
  );
}

const CATEGORY_ORDER = [
  "Introduction",
  "Concepts",
  "Application",
  "Interactions",
  "Layout",
  "...",
  "Content",
  "Internationalization",
  "Server Side Rendering",
  "Utilities",
];

function Nav({ currentPageName, pages }) {
  let isIndex = /index\.html$/;
  let currentParts = currentPageName.split("/");
  let isBlog = isBlogSection(currentParts[0]);
  let blogIndex = currentParts[0] + "/index.html";
  if (isBlog) {
    currentParts.shift();
  }

  let currentDir = currentParts[0];

  pages = pages.filter((p) => {
    let pageParts = p.name.split("/");
    let pageDir = pageParts[0];
    if (isBlogSection(pageDir)) {
      return (
        currentParts.length === 1 &&
        pageParts[pageParts.length - 1] === "index.html"
      );
    }

    // Skip the error page, its only used for 404s
    if (p.name === "error.html") {
      return false;
    }

    // Pages within same directory (react-spectrum/Alert.html)
    if (currentParts.length > 1) {
      return currentDir === pageDir && !isIndex.test(p.name);
    }

    // Top-level index pages (react-spectrum/index.html)
    if (
      currentParts.length === 1 &&
      pageParts.length > 1 &&
      isIndex.test(p.name)
    ) {
      return true;
    }

    // Other top-level pages
    return !isIndex.test(p.name) && pageParts.length === 1;
  });

  if (currentParts.length === 1) {
    pages.push({
      category: "Spectrum Ecosystem",
      name: "spectrum-design",
      title: "Spectrum Design",
      url: "https://spectrum.adobe.com",
    });
    pages.push({
      category: "Spectrum Ecosystem",
      name: "spectrum-css",
      title: "Spectrum CSS",
      url: "https://opensource.adobe.com/spectrum-css/",
    });
  }

  // Key by category
  let pageMap = {};
  let rootPages = [];
  pages.forEach((p) => {
    let cat = p.category;
    if (cat) {
      if (cat in pageMap) {
        pageMap[cat].push(p);
      } else {
        pageMap[cat] = [p];
      }
    } else {
      rootPages.push(p);
    }
  });

  // Order categories so specific ones come first, then all the others in sorted order.
  let categories = [];
  for (let category of CATEGORY_ORDER) {
    if (pageMap[category]) {
      categories.push(category);
    } else if (category === "...") {
      for (let category of Object.keys(pageMap).sort()) {
        if (!CATEGORY_ORDER.includes(category)) {
          categories.push(category);
        }
      }
    }
  }

  let title =
    currentParts.length > 1 ? dirToTitle(currentPageName) : "React Spectrum";
  let currentPageIsIndex = isIndex.test(currentPageName);

  function SideNavItem({ name, url, title, preRelease }) {
    const isCurrentPage = !currentPageIsIndex && name === currentPageName;
    return (
      <li
        className={clsx(sideNavStyles["spectrum-SideNav-item"], {
          [sideNavStyles["is-selected"]]:
            isCurrentPage || (name === blogIndex && isBlog),
        })}
      >
        <a
          className={clsx(
            sideNavStyles["spectrum-SideNav-itemLink"],
            docStyles.sideNavItem
          )}
          href={url}
          aria-current={isCurrentPage ? "page" : null}
          {...getAnchorProps(url)}
        >
          {title}
          <VersionBadge version={preRelease} />
        </a>
      </li>
    );
  }

  return (
    <nav className={docStyles.nav} aria-labelledby="nav-title-id">
      <header>
        {currentParts.length > 1 && (
          <a href="../index.html" className={docStyles.backBtn}>
            <ChevronLeft aria-label="Back" />
          </a>
        )}
        <a
          href={isBlog ? "/index.html" : "./index.html"}
          className={docStyles.homeBtn}
          id="nav-title-id"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 532.43 532.44"
            width="1em"
            height="1em"
          >
            <g data-name="Layer 2">
              <g data-name="Layer 1">
                <g fill="#222">
                  <path d="M388.67 4.49c-4.56 2.73-7.22 6.23-6.08 7.78 1.52 1.56-.38 2.73-4.57 3.11-6.08 0-5.7 1.17 2.28 4.67 18.25 8.94 27.38 17.12 22.44 20.62-3.42 1.95-2.66 3.51 2.67 5.45 10.64 4.28 12.92 1.17 12.92-17.12 0-11.28-2.28-18.67-6.84-22.95-8.36-7.79-11.41-7.79-22.82-1.56zM123.6 26.66c-6.47 7.4-6.84 10.12-2.27 26.46 4.93 20.22 12.55 30.34 19.39 26.07 3.05-2 2.66-3.89-1.9-7.39-5.33-3.89-5.33-6.23-1.15-16.74 4.18-9.33 6.47-11.66 10.65-8.94 14.83 9.72 15.59-3.89 1.52-17.51-12.55-11.67-17.12-12.05-26.24-1.95zM339.61 53.9c-10.26 10.9-12.93 11.67-57.81 15.56-26.23 2.33-48.3 4.66-49.06 5.83-1.14 1.16-4.18 0-6.84-2.33-3.42-2.72-6.09-1.56-10.65 4.67-6.08 9.37-4.94 15.17 3.05 15.17 6.46 0 11 16 6.08 21-4.57 4.67-4.94 29.56-.39 29.56s8.75-29.18 5.71-41.24C227 92 227.43 92 235.41 99c5.33 4.67 8 11.67 8 20.23s1.52 12.06 4.18 10.11c2.66-1.56 3.42-9.72 2.28-22.56-2.28-19.46-2.28-19.46 3-8.18 3.43 7.4 4.57 17.52 3.43 27.62-1.91 12.45-.76 16.74 3.8 18.29 3.42 1.56 6.08 1.16 6.47-.39 0-1.56 1.14-11.28 1.9-21.78 1.9-21.38 7.22-33.45 14.07-33.45 3.05 0 3.42 1.17.39 3.11-7.6 5.45-9.13 17.12-5.33 42 4.94 31.13 2.28 44.36-9.5 44.36-4.57 0-11.41 3.5-15.22 7.77-7.6 9-17.11 4.29-17.11-8.17 0-10.12-6.85-12.84-8.37-2.72-.76 4.67-3.81 8.94-6.85 10.11-4.56 1.56-4.94.78-.76-3.5 2.28-2.72 4.57-10.5 4.57-17.12 0-9.72-2.28-13.61-9.89-17.11-12.93-5.84-20.91-3.11-31.19 10.89l-8 10.9 11.79 12.45C195.49 190.84 202 194 211.46 194c7.23 0 12.93 1.56 12.93 3.89 0 5.84-16.73 4.67-28.9-1.94-11.79-6.23-20.54-7.4-20.54-3.11.39 5.45 9.51 15.17 17.12 18.67 6.84 2.73 6.84 3.51.76 4.67-11.41 3.11-7.23 10.12 4.94 8.18 8.74-1.56 11.41-.39 10.65 4.28-.76 3.51 1.14 6.23 3.8 5.84 2.28-.39 8-.78 12.17-1.17 4.56 0 7.6-3.11 7.6-7.39 0-12.85 4.57-12.45 15.59 1.56 9.5 12.06 10.26 15.17 6.47 24.12a105 105 0 01-10.27 17.52c-5.32 6.61-4.56 6.61 11.41 4.28 20.91-3.89 25.86-3.89 36.89 0 11.79 3.89 11-4.67-1.15-9.34-5.32-1.94-11.4-8.17-14.07-13.61-3.42-8.17-3-10.9 2.67-17.51 6.84-7.78 14.82-8.56 15.58-1.56 1.91 12.84 7.23 21 13.31 21 4.18 0 7.23 2.73 7.23 5.84s1.9 5.83 3.81 5.83a3.86 3.86 0 003.8-3.89c0-2.72 2.28-2.72 7.23 0 9.13 5.06 14.46-1.56 8-9.72-3.81-4.67-2.28-5.45 11.41-3.89 20.53 1.94 22.81 4.67 9.5 9.72-10.26 3.89-10.26 4.67-4.57 10.51 4.94 5.05 7.23 5.44 11 1.94a16.64 16.64 0 0110.68-4.72c14.45 0 35-35 45.64-77 3.42-14 3-19.06-4.18-35-14.46-32.68-15.59-40.07-8.75-61.47 8.75-29.18 6.84-35-12.92-33.45-10.27.78-16-.39-16-3.11 0-4.67-10.26-3.89-30.42 2.33-9.51 2.72-9.51 2.72-2.28-5.45 11.03-12.85 4.19-10.18-7.99 3.05zm16 19.46c-1.14 1.94-.38 3.88 1.91 3.88s3 1.56 1.9 3.89c-1.52 2.33-4.18 2-7.23-1.16-5.71-5-4.56-10.5 1.91-10.5 2.24 0 2.63 1.53 1.48 3.89zm31.19 26.06c1.14 3.5.76 12.84-1.52 21C383 129 383 137.54 384.87 141c2.66 4.67 1.14 7-6.85 10.12-9.5 3.89-9.88 4.67-3.8 11.28 8.37 9.73 7.6 16-2.28 16-4.94 0-17.49 4.66-28.52 10.5-16 8.56-20.92 9.72-24.72 5.83-3.42-3.5-2.66-4.67 4.94-4.67 12.92 0 24.34-8.94 30-23.73 4.94-11.66 4.57-12.84-5.32-21.4-19-17.12-40.69-8.94-48.31 17.9-4.18 13.61-10.65 21-11.4 12.45-3.43-46.69-3-75.1 1.14-73.92 2.66 1.16 6.47 9.34 8.37 19.06 4.18 20.62 5.32 22.57 9.89 15.17 1.9-3.11 2.28-13.22 1.14-22.17-1.51-11.28-.76-16.73 2.28-16.73a4 4 0 014.18 3.89c0 1.94 2.28 3.89 5.33 3.89 7.6 0 10.26 10.5 3.8 15.56q-13.11 11.66 8 11.66c10.65 0 13.31-1.56 13.31-7.39 0-3.89-2.66-9.34-5.71-12.06s-5.71-6.23-5.71-8.18c0-5.83 18.25-3.89 24.35 2.73 7.6 7.39 14.82 3.5 11.79-6.62-1.91-5.83-.39-7.39 5.71-7.39 4.21.02 8.77 2.75 10.29 6.64zm-36.88 58.35c-.39 7-6.85 13.62-9.14 9.34a9 9 0 01-.38-8.17c2.6-4.67 9.49-5.05 9.49-1.17zm-148.36 12.85c0 1.94-.76 3.89-1.51 3.89-1.15 0-3.05-1.95-4.18-3.89-1.15-2.33-.39-3.89 1.51-3.89a3.94 3.94 0 014.18 3.89zm159.73 31.12a3.85 3.85 0 01-3.8 3.89 4.13 4.13 0 01-3.81-3.89 3.86 3.86 0 013.81-3.89 3.63 3.63 0 013.8 3.89zm30 11.67c3.42 10.12 3 12.45-2.28 14.39-3.42 1.56-7.23.39-8-1.94-.76-2.73-8-4.67-16.36-4.67-14.07 0-22-7.78-8-7.78 3.42-.39 10.65-2.73 16-5.83 12.98-7.78 14.12-7.4 18.69 5.83zm-33.85 19.06c0 4.67-8 7.39-14.07 5.05-8.37-3.1-5.33-8.55 4.56-8.55 5.38.03 9.56 1.56 9.56 3.5zM136.91 96.69c2.28 4.29 3.05 9.72 1.91 11.67-1.15 2.33-1.15 3.89.76 3.89 3.42 0 12.55 18.67 12.55 26.07 0 2.72 2.66 3.89 5.71 2.72 6.08-2.33 6.08-17.51 0-22.17-2.28-1.56-1.91-4.28 1.9-6.23 3-2.33 4.56-4.28 3.81-5-1.15-.78-5.33-3.11-9.51-5.83-4.18-2.33-7.23-6.23-6.47-8.56.39-2.33-2.66-4.29-6.84-4.29-6.86-.05-7.24 1.12-3.82 7.73z" />
                  <path d="M195.85 131.71c-1.14 2.33.39 5.06 3.81 6.23 7.23 3.11 11.41 0 7.6-5.83-3.03-5.46-8.36-5.46-11.41-.4zM140.33 169.45c-8 12.85-8.37 15.56-4.18 35 2.28 11.28 7.61 26.07 12.17 32.28 4.18 6.23 7.23 12.45 6.84 14.4-.38 1.56 3.05 11.28 7.61 21.4 8.74 18.28 25.86 34.63 41.83 39.68 11.41 3.89 11-4.67-.76-9.34C193.19 299 178 280.33 181 274.88c1.14-1.95-.39-7.78-3.81-12.85-4.57-7-4.57-9.72-1.14-10.89 2.27-1.17-1.91-4.67-9.51-8.18-11-4.67-14.45-8.56-14.45-15.55 0-11.67 3.8-22.17 7.22-20.23 1.15.78 2.28-2.33 2.28-6.61a119.5 119.5 0 013-18.68c1.9-8.56 1.14-10.9-4.94-12.45-4.94-1.16-6.84-3.89-5.33-8.17 4.68-11.66-5.24-6.22-13.99 8.18z" />
                  <path d="M186.35 234.81c-2.66 3.11-3 5.83-.76 5.83 1.9 0 5.71-2.72 8.37-5.83s3-5.83.76-5.83c-1.9.02-5.72 2.71-8.37 5.83zM214.87 244.53c-3 5.06 5.71 5.06 13.32 0 4.56-3.11 4.18-3.89-2.67-3.89-4.52 0-9.52 1.56-10.65 3.89zM325.54 286.94c-12.55 9.72-16 14.39-11.79 15.17 3.42.78 8.74-.78 11.79-3.5 4.94-3.89 6.47-3.89 9.13.38 1.9 3.51 1.9 7.78-.76 10.51-5.32 6.61-3 6.23 17.49-2.73 14.07-6.61 17.49-10.12 17.49-17.51 0-10.9-10.26-13.61-13.69-3.89-1.52 3.11-4.57 5.83-7.6 5.83-6.85 0-6.85-3.5.38-11.66 10.65-12.06-3.8-7.39-22.44 7.4zM165.44 332.07c-12.55 12.85-25.11 19.46-70.36 36.58-30.43 11.66-64.28 25.29-74.93 30L0 407.55v120.88a4 4 0 004 4h254.6v-9.72c0-5.45 1.91-9.73 4.18-9.73s3.05-2.72 1.52-7c-1.9-5.45-1.52-5.84 2.28-1.95 11.41 11.67 11.41 3.89 0-8.17-10.65-11.29-12.17-15.18-11-29.57l1.11-16.29 11.41 8.94 11.4 8.94-11-12.45c-10.26-11.28-9.5-18.28.76-7.77 2.66 2.72 4.57 3.11 4.57.78s-3.43-7.4-7.61-11.29c-6.46-6.23-7.6-10.12-5.32-24.89 1.14-9.73 4.18-19.84 6.08-21.79 2.67-2.72 3.05 1.95 1.52 13.61-1.52 9.34-1.52 16 0 14.4s4.94 1.94 7.6 7.77l4.18 10.9 3.05-11.28c2.28-7.78 1.14-14.39-3.42-22.57-6.47-10.89-6.47-11.28 2.28-18.67 11.79-10.12 13.31-9.73 20.15 3.89 8.37 16.33 10.65 14.39 3.81-2.73-11.41-28-19-37.74-31.95-41.62-10.26-3.11-13.7-2.73-17.49 2.72-2.28 3.5-5.71 5.45-6.84 4.28-2.67-3.11-12.56 13.62-15.59 26.07-1.52 8.18-.76 10.9 5.32 12.45 13.7 3.51 12.93 19.06-1.52 41.63-8 12.45-13.69 26.07-13.69 32.68 0 17.9-5.71 8.18-16.73-28-11-35.8-11.8-41.24-5.71-37.35 2.27 1.16 1.14-1.95-2.28-7.78-3.81-5.45-9.5-24.51-12.93-42.41l-6.08-31.9zm116 23.35c0 1.16-1.52 5.05-3.42 8.94a81.86 81.86 0 00-6.09 14.39c-2.66 7-3.42 6.62-8-3.89-4.18-10.11-4.18-12.44 1.52-16.73 6.09-4.67 15.98-6.61 15.98-2.71z" />
                  <path d="M350.26 323.51c-.39 10.12-20.15 99.6-31.56 140.84l-4.18 15.55-4.94-24.9c-5.33-26.46-17.12-46.3-16-27.24a222.23 222.23 0 010 22.57c-.76 12.84-.76 13.23-8.38 6.23-4.18-3.89-7.6-5.05-7.6-2.73a8 8 0 003.42 6.23c4.18 2.73 9.13 23.73 9.51 44 .38 13.23-.39 15.56-4.57 12.06s-4.18-2.72-.38 5.84l4.56 10.5h238.29a4 4 0 004-4V403.67l-23.58-10.9c-13.31-6.23-46-18.68-73.39-27.62-57.81-19.06-76.44-28.78-81.38-42.41l-3.43-10.11zM209.92 411.82c2.67 4.29 6.47 7.78 8.38 7.78s.38-3.5-3.43-7.78-7.6-7.78-8.37-7.78.76 3.51 3.42 7.78z" />
                </g>
                <path
                  d="M50.62 500.12H50v-2.39a4.74 4.74 0 00-2-1.73 6.86 6.86 0 00-3-.59 5.88 5.88 0 00-3.67 1 3 3 0 00-.24 4.9 10.59 10.59 0 004.05 1.71 12.43 12.43 0 014.28 1.84 3.59 3.59 0 011.44 3.14 3.72 3.72 0 01-1.65 3.2 7 7 0 01-4.14 1.16 8.64 8.64 0 01-3.29-.64 6 6 0 01-2.61-2v-2.81h.63v2.55a5.28 5.28 0 002.33 1.73 8.23 8.23 0 002.94.54 6.29 6.29 0 003.7-1 3.28 3.28 0 00.27-5.24 11.66 11.66 0 00-4.04-1.81 13.14 13.14 0 01-4.27-1.79 3.4 3.4 0 01-1.4-2.89 3.57 3.57 0 011.59-3.05 6.91 6.91 0 014.08-1.16 8 8 0 013.36.66 5.21 5.21 0 012.27 2zM66 495v3.44h-.64v-2.8h-5.55v15.79h1.91v.64h-4.45v-.64h1.91v-15.76H53.6v2.8H53V495zM65.53 511.46h1.26L72.93 495h.77l6.15 16.43h1.26v.64h-3.62v-.64h1.67L77.49 507h-8.35l-1.66 4.46h1.67v.64h-3.62zm3.85-5.1h7.87l-3.57-9.6-.33-1h-.07l-.32 1zM94 495v3.44h-.63v-2.8h-5.56v15.79h1.91v.64h-4.45v-.64h1.91v-15.76H81.6v2.8H81V495zM95.83 512.1v-.64h1.91v-15.79h-1.91V495h12.29v3.44h-.63v-2.8h-9.12v7.21h7.38v.64h-7.38v7.94h9.12v-2.8h.63v3.44zM117.47 495a6.47 6.47 0 014.15 1.17 4.48 4.48 0 01.53 6 4.17 4.17 0 01-2.51 1.53 4.05 4.05 0 012.64 1.35 4.21 4.21 0 01.79 2.64v1.72a2.64 2.64 0 00.35 1.5 1.16 1.16 0 001 .49h.6v.64h-.6a1.66 1.66 0 01-1.57-.77 3.76 3.76 0 01-.43-1.88v-1.68a3.32 3.32 0 00-1.19-2.71 5.27 5.27 0 00-3.39-1h-4.61v7.38h1.91v.64h-4.45v-.64h1.91v-15.7h-1.91V495h6.76zm-4.22 8.42h4.1a5.67 5.67 0 003.85-1.1 3.85 3.85 0 000-5.67 5.83 5.83 0 00-3.72-1h-4.22zM126.51 511.46h1.25L133.9 495h.77l6.16 16.43h1.25v.64h-3.62v-.64h1.67l-1.67-4.43h-8.34l-1.67 4.46h1.68v.64h-3.62zm3.84-5.1h7.87l-3.57-9.6-.33-1h-.07l-.31 1zM143.94 512.1v-.64h1.91v-15.79h-1.91V495h12.3v3.44h-.64v-2.8h-9.11v7.21h7.38v.64h-7.38v7.94h9.11v-2.8h.64v3.44zM391.68 495.67H390l5.49 14.76.31.93h.07l.3-.93 5.5-14.76H400V495h3.62v.64h-1.25l-6.14 16.43h-.77l-6.14-16.43h-1.26V495h3.62zM405.57 495.67V495H410v.64h-1.91v15.79H410v.64h-4.45v-.64h1.91v-15.76zM428.25 495v.64h-1.91v16.46h-.64l-10.94-15.9h-.07v15.23h1.91v.64h-4.47v-.64H414v-15.76h-1.91V495h2.56l10.94 15.86h.07v-15.19h-1.91V495h4.46zM443.8 500.47h-.65v-2.74a5.33 5.33 0 00-2.1-1.68 6.79 6.79 0 00-3-.63 6.22 6.22 0 00-4.94 2.21 8.65 8.65 0 00-1.9 5.81v.23a8.67 8.67 0 001.89 5.82 6.06 6.06 0 004.83 2.22 7.28 7.28 0 003.06-.63 5.36 5.36 0 002.14-1.68v-2.74h.65v3a5.76 5.76 0 01-2.42 2 7.93 7.93 0 01-3.43.73 6.63 6.63 0 01-5.3-2.41 9.29 9.29 0 01-2-6.26v-.21a9.31 9.31 0 012.06-6.27 6.8 6.8 0 015.41-2.4 7.65 7.65 0 013.35.72 6.05 6.05 0 012.38 2zM446.45 495.67V495h4.45v.64H449v15.79h1.91v.64h-4.45v-.64h1.91v-15.76zM466.17 495v3.44h-.64v-2.8H460v15.79h1.91v.64h-4.46v-.64h1.91v-15.76h-5.57v2.8h-.64V495z"
                  fill="#f3f4f6"
                />
              </g>
            </g>
          </svg>
          <h2 className={typographyStyles["spectrum-Heading4"]}>{title}</h2>
        </a>
      </header>
      <ul className={sideNavStyles["spectrum-SideNav"]}>
        {rootPages.map((p) => (
          <SideNavItem {...p} />
        ))}
        {categories.map((key) => {
          const headingId = `${key
            .trim()
            .toLowerCase()
            .replace(/\s+/g, "-")}-heading`;
          return (
            <li className={sideNavStyles["spectrum-SideNav-item"]}>
              <h3
                className={sideNavStyles["spectrum-SideNav-heading"]}
                id={headingId}
              >
                {key}
              </h3>
              <ul
                className={sideNavStyles["spectrum-SideNav"]}
                aria-labelledby={headingId}
              >
                {pageMap[key]
                  .sort((a, b) =>
                    (a.order || 0) < (b.order || 0) || a.title < b.title
                      ? -1
                      : 1
                  )
                  .map((p) => (
                    <SideNavItem {...p} />
                  ))}
              </ul>
            </li>
          );
        })}
      </ul>
    </nav>
  );
}

function Footer() {
  const year = new Date().getFullYear();
  return (
    <footer className={docStyles.pageFooter}>
      <hr
        className={clsx(
          ruleStyles["spectrum-Rule"],
          ruleStyles["spectrum-Rule--small"],
          ruleStyles["spectrum-Rule--horizontal"]
        )}
      />
      <ul>
        <li>Copyright © {year} Watheia Labs, LLC. All rights reserved.</li>
        <li>
          <a
            className={clsx(
              linkStyle["spectrum-Link"],
              linkStyle["spectrum-Link--secondary"],
              docStyles.link
            )}
            href="//cdn.watheia.org/assets/terms-and-conditions.txt"
          >
            Privacy
          </a>
        </li>
        <li>
          <a
            className={clsx(
              linkStyle["spectrum-Link"],
              linkStyle["spectrum-Link--secondary"],
              docStyles.link
            )}
            href="//cdn.watheia.org/assets/terms-and-conditions.txt"
          >
            Terms of Use
          </a>
        </li>
        <li>
          <a
            className={clsx(
              linkStyle["spectrum-Link"],
              linkStyle["spectrum-Link--secondary"],
              docStyles.link
            )}
            href="//cdn.watheia.org/assets/terms-and-conditions.txt"
          >
            Cookies
          </a>
        </li>
      </ul>
    </footer>
  );
}

export const PageContext = React.createContext();
export function BaseLayout({
  scripts,
  styles,
  pages,
  currentPage,
  publicUrl,
  children,
  toc,
}) {
  return (
    <Page
      scripts={scripts}
      styles={styles}
      publicUrl={publicUrl}
      currentPage={currentPage}
    >
      <div style={{ isolation: "isolate" }}>
        <header className={docStyles.pageHeader} />
        <Nav currentPageName={currentPage.name} pages={pages} />
        <main>
          <MDXProvider components={mdxComponents}>
            <ImageContext.Provider value={publicUrl}>
              <LinkProvider>
                <PageContext.Provider value={{ pages, currentPage }}>
                  {children}
                </PageContext.Provider>
              </LinkProvider>
            </ImageContext.Provider>
          </MDXProvider>
          {toc.length ? <ToC toc={toc} /> : null}
          <Footer />
        </main>
      </div>
    </Page>
  );
}

export function Layout(props) {
  return (
    <BaseLayout {...props}>
      <article
        className={clsx(
          typographyStyles["spectrum-Typography"],
          docStyles.article,
          {
            [docStyles.inCategory]: !props.currentPage.name.endsWith(
              "index.html"
            ),
          }
        )}
      >
        <VersionBadge version={props.currentPage.preRelease} size="large" />
        {props.children}
      </article>
    </BaseLayout>
  );
}

export function BlogLayout(props) {
  return (
    <BaseLayout {...props}>
      <div
        className={clsx(
          typographyStyles["spectrum-Typography"],
          docStyles.article,
          docStyles.inCategory
        )}
      >
        {props.children}
      </div>
    </BaseLayout>
  );
}

export function BlogPostLayout(props) {
  // Add post date underneath the h1
  let date = props.currentPage.date;
  let author = props.currentPage.author || "";
  let authorParts = author.match(/^\[(.*?)\]\((.*?)\)$/) || [""];
  let components = mdxComponents;
  if (date) {
    components = {
      ...mdxComponents,
      h1: (props) => (
        <header className={docStyles.blogHeader}>
          {mdxComponents.h1(props)}
          {author && (
            <address className={typographyStyles["spectrum-Body4"]}>
              By{" "}
              <a
                rel="author"
                href={authorParts[2]}
                className={clsx(linkStyle["spectrum-Link"], docStyles.link)}
                {...getAnchorProps(authorParts[2])}
              >
                {authorParts[1]}
              </a>
            </address>
          )}
          <Time date={date} />
        </header>
      ),
    };
  }

  return (
    <Layout {...props}>
      <MDXProvider components={components}>{props.children}</MDXProvider>
    </Layout>
  );
}

export function Time({ date }) {
  // treat date as local time rather than UTC
  let localDate = new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate()
  );
  return (
    <time
      dateTime={date.toISOString().slice(0, 10)}
      className={typographyStyles["spectrum-Body4"]}
    >
      {localDate.toLocaleString("en-US", {
        year: "numeric",
        month: "long",
        day: "numeric",
      })}
    </time>
  );
}
