/*
 * Copyright 2021 Watheia Labs, LLC. All rights reserved.
 * This file is licensed to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR REPRESENTATIONS
 * OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

import {getIllustrationProps, IllustrationProps} from './utils';
import React from 'react';

export default function Error404(props: IllustrationProps) {
  return (
    <svg width="135.321" height="87" {...getIllustrationProps(props)}>
      <g fill="none" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10">
        <path d="M11.821 60.5v23a2.006 2.006 0 002 2h118a2.006 2.006 0 002-2v-80a2.006 2.006 0 00-2-2h-118a2.006 2.006 0 00-2 2v27" strokeWidth="3" />
        <path strokeWidth="2" d="M133.721 14h-122M29.721 8h-10" />
        <path strokeWidth="3" d="M2.121 55.1l19.3-19.2M21.421 55.1l-19.3-19.2" />
      </g>
    </svg>
  );
}
