/*
 * Copyright 2021 Watheia Labs, LLC. All rights reserved.
 * This file is licensed to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR REPRESENTATIONS
 * OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

import {getIllustrationProps, IllustrationProps} from './utils';
import React from 'react';

export default function Error504(props: IllustrationProps) {
  return (
    <svg width="137" height="94" {...getIllustrationProps(props)}>
      <g transform="translate(-404.3 -3780.5)" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10">
        <path d="M417.8 3803v-19a2.006 2.006 0 012-2h118a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-102M435.8 3814h102a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-102M435.8 3846h102a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-118a2.006 2.006 0 01-2-2v-17M527.8 3842v3M527.8 3810v3" strokeWidth="3" />
        <circle cx="4" cy="4" r="4" transform="translate(523.8 3791.5)" strokeWidth="2" />
        <circle cx="4" cy="4" r="4" transform="translate(523.8 3824)" strokeWidth="2" />
        <circle cx="4" cy="4" r="4" transform="translate(523.8 3855.5)" strokeWidth="2" />
        <path d="M429.8 3844.4v3.6h-24v-3.6c0-9.1 9-10.9 9-15.4v-.9c0-4.5-9-6.3-9-15.4v-3.6h24v3.6c0 9.1-9 10.9-9 15.4v.9c0 4.5 9 6.3 9 15.4z" strokeWidth="3" />
        <path d="M428.6 3846.8l-8.4-8.4a3.441 3.441 0 00-4.8 0l-8.4 8.4" strokeWidth="2" />
      </g>
    </svg>
  );
}
