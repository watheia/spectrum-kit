/*
 * Copyright 2021 Watheia Labs, LLC. All rights reserved.
 * This file is licensed to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR REPRESENTATIONS
 * OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

import {getIllustrationProps, IllustrationProps} from './utils';
import React from 'react';

export default function Error503(props: IllustrationProps) {
  return (
    <svg width="143.1" height="94" {...getIllustrationProps(props)}>
      <g transform="translate(-416.2 -3367.5)" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" fill="none">
        <circle cx="18.1" cy="18.1" r="18.1" transform="translate(417.7 3396.9)" strokeWidth="3" />
        <path strokeWidth="3" d="M423.4 3402.2l24.5 24.4M435.8 3390v-19a2.006 2.006 0 012-2h118a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-104M457.8 3401h98a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-97M452.8 3433h103a2.006 2.006 0 012 2v23a2.006 2.006 0 01-2 2h-118a2.006 2.006 0 01-2-2v-18M546.8 3429v3M546.8 3397v3" />
        <circle cx="4" cy="4" r="4" transform="translate(542.8 3378.5)" strokeWidth="2" />
        <circle cx="4" cy="4" r="4" transform="translate(542.8 3411)" strokeWidth="2" />
        <circle cx="4" cy="4" r="4" transform="translate(542.8 3442.5)" strokeWidth="2" />
      </g>
    </svg>
  );
}
